import sys
from src.functions import some_function
sys.path.append('..')

def test_some_function():
    number = some_function()
    assert isinstance(number, int)