import random as rand

def some_function():
    number = rand.randint(10,1000)
    print(f'This is the submodule function and the number is {number}.')
    return number
    